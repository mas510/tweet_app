class PostsController < ApplicationController
  def index
    @posts = Post.all
    @posts = Post.order(created_at: :desc)
  end
  def show
    @id = params[:id]
    @post = Post.find_by(id:@id)
  end
  def new
  end
  def create
    @content = params[:content]
    @post = Post.new(content:@content)
    @post.save

    redirect_to("/posts/index")
  end
end
